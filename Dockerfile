ARG UBUNTU_RELEASE_VERSION=18.04

ARG OPENSSL_RELEASE_VERSION=1.1.1g
ARG OPENSSL_RELEASE_DOWNLOAD_LINK="https://www.openssl.org/source/openssl-${OPENSSL_RELEASE_VERSION}.tar.gz"
FROM ubuntu:${UBUNTU_RELEASE_VERSION}
RUN apt-get update
RUN apt-get install -y wget
RUN wget $OPENSSL_RELEASE_DOWNLOAD_LINK
RUN ls -l


FROM ubuntu:${UBUNTU_RELEASE_VERSION}



RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install build-essential checkinstall zlib1g-dev -y
WORKDIR /usr/local/src/
#ADD ${OPENSSL_RELEASE_DOWNLOAD_LINK} .
COPY ./openssl-1.1.1g.tar.gz .
RUN ls -l
RUN tar -xf openssl-1.1.1g.tar.gz

WORKDIR /usr/local/src/openssl-1.1.1g
RUN ./config --prefix=/usr/local/ssl --openssldir=/usr/local/ssl shared zlib
RUN make
RUN make test
RUN make install

WORKDIR /etc/ld.so.conf.d/
RUN echo "/usr/local/ssl/lib" > openssl-1.1.1c.conf
RUN ldconfig -v

RUN echo $PATH:/usr/local/ssl/bin > /etc/environment
RUN source /etc/environment
RUN echo $PATH
RUN openssl version -a

#RUN tar -xf openssl-${OPENSSL_RELEASE_VERSION}.tar.gz
